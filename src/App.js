import React from 'react'
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact"
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../node_modules/bootstrap/dist/js/bootstrap.bundle'
import placesData from './placesData.js'
import saloonData from './saloonData.js'
import logo from './assests/lalogo1.png'
import headerimage from './assests/headerimage.png'
import cover from './assests/cover.png'
import image from './assests/california.png'
import placesdata from './placesData.js'
import { Link } from 'react-router-dom'
import searchButton from './assests/searchButton.png'
import { NavLink } from 'react-bootstrap'
function App() {
	// PLACES DATA
	const Renderplaces = placesData.map(place => {
		return (<>
			<div className="col-6 col-sm-4 col-lg-3 mt-3">
				<img src={place.image} alt="description" className="mr-2"></img>
				{place.name}

			</div>
		</>
		)

	})
	// SALOON DATA
	const RenderSaloon = saloonData.map(saloon => {
		return (

			<>

				<div className="col-4 mt-3">
					<div className="d-inline-flex">
						<img src={saloon.image} alt="description" className="mr-4"></img>
						<div>
							<p className="mb-0 mr-2">{saloon.name}</p>
							<p className="mb-0 mr-2">{saloon.address}</p>
							<p className="mb-0 mr-2">{saloon.rating}</p>
							<p className="mb-0 mr-2">{saloon.numReviews}</p>
						</div>
					</div>
				</div>

			</>
		)

	})

	return (
		<>

			<div className="p-5 text-center bg-image  BackgroundImage img-fluid "
				style={{ backgroundImage: `url(${headerimage})` }}
			>

				<nav className="navbar fixed-top navbar-expand-md   ">

					<NavLink className="navbar-brand col-6" href="/">
						<img src={logo} alt="" />
					</NavLink>
					<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
						<span className="navbar-toggler-icon"></span>
					</button>
					<div className="collapse navbar-collapse" id="navbarResponsive"  >
						<ul className="navbar-nav ml-auto">
							<li className="nav-item active">
								<a className="nav-link" href="#">Home
                <span className="sr-only">(current)</span>
								</a>
							</li>
							<li className="nav-item">
								<a className="nav-link" href="#">About</a>
							</li>
							<li className="nav-item">
								<a className="nav-link" href="#">Services</a>
							</li>
							<li className="nav-item">
								<a className="nav-link" href="#">Contact</a>
							</li>
							<li className="nav-item">
								<a className="nav-link" href="#">Signin</a>
							</li>
						</ul>

					</div>

				</nav>
				<div className="mask">
					<div className="d-flex justify-content-center align-items-center h-100">
						<div className="text-white">
							<h3 className="mb-3">BOOK YOUR BARBER NOW!</h3>
							<p className="mb-3">This is dummy text</p>
						</div>

					</div>
					<div className="input-group  col-12 col-md-6 mx-auto">
						<input type="search" class="form-control a" placeholder="Find" aria-label="Search" aria-describedby="search-addon" />
						<input type="search" class="form-control a " placeholder="Location" aria-label="Search" aria-describedby="search-addon" />

						<button className="" type="submit">
							<img src={searchButton}></img>
						</button>
					</div>
				</div>
			</div>

			<div className="row col-12">
				{Renderplaces}
			</div>
			<br></br>
			<h1 className="text-center">Featured Spa's and Saloons</h1>
			<div className="row col-12">
				{RenderSaloon}
			</div>
			<br></br>
			{/* Cover Photo */}
			<div className="p-5 text-center bg-image  BackgroundImage img-fluid "
				style={{ backgroundImage: `url(${cover})` }}
			>
				<div className="d-flex justify-content-center align-items-center h-100">
					<div className="text-white align-content-center">
						<h2 className="col-9">List Your Salon or Spa's Business & Start Getting Onine Booking</h2>
					</div>
					<button className="btn-light button" type="submit">
						List Now
						</button>
				</div>
			</div>
			<MDBFooter color="blue" className="font-small pt-4 mt-4">
				<MDBContainer fluid className="text-center text-md-left">
					<MDBRow>
						<MDBCol md="3">
							<div >
								<img src={logo} alt="" />
							</div>

							<p>
								Here you can use rows and columns here to organize your footer
								content.
            </p>
						</MDBCol>
						<MDBCol md="3">

							<ul>
								<li className="list-unstyled">
									<a href="#!">About Us</a>
								</li>
								<li className="list-unstyled">
									<a href="#!">Contact Us</a>
								</li>
								<li className="list-unstyled">
									<a href="#!">Privacy Policy</a>
								</li>
								<li className="list-unstyled">
									<a href="#!">Careers</a>
								</li>
								<li className="list-unstyled">
									<a href="#!">Covid-19</a>
								</li>
							</ul>
						</MDBCol>

						<MDBCol md="3">
							{/* <h5 className="title">Links</h5> */}
							<ul>
								<li className="list-unstyled">
									<a href="#!">List Your Business</a>
								</li>
								<li className="list-unstyled">
									<a href="#!">How to list your Business</a>
								</li>
								<li className="list-unstyled">
									<a href="#!">News Letter</a>
								</li>

							</ul>
						</MDBCol>

						<MDBCol md="3">
							<h5 className="title">Subscribe to News Letter</h5>
							<ul>
								<input type="search" class="form-control a " placeholder="Email" aria-label="Search" aria-describedby="search-addon" />

								<button className="butt" type="submit">
									Subscribe
								</button>

							</ul>
						</MDBCol>
					</MDBRow>
				</MDBContainer>
				<div className="footer-copyright text-center py-3">
					<MDBContainer fluid>
						{new Date().getFullYear()} Copyright &copy; <a href="Blackbooking.org"> Blackbooking.org </a>
					</MDBContainer>
				</div>
			</MDBFooter>

		</>

	);
}

export default App;
